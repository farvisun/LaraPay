<div class="right-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li><a class="has-arrow  " href="#" aria-expanded="false"><i class="fas fa-tachometer-alt"></i>
                        <span
                                class="hide-menu">پیشخوان
                        </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('admin.dashboard.index') }}">وضعیت </a></li>
                        <li><a href="{{ route('admin.dashboard.statistics') }}">آمار </a></li>
                    </ul>
                </li>
                <li><a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-users"></i><span
                                class="hide-menu">کاربران</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('admin.users.index') }}">لیست کاربران</a></li>
                        <li><a href="{{ route('admin.users.create') }}">کاربر جدید</a></li>
                        <li><a href="{{ route('admin.users.accounts.index') }}">حساب های بانکی</a></li>
                        <li><a href="{{ route('admin.users.accounts.create') }}">حساب بانکی جدید</a></li>
                    </ul>
                </li>
                <li><a class="has-arrow  " href="#" aria-expanded="false">
                        <i class="fas fa-lightbulb"></i>
                        <span
                                class="hide-menu">پلن ها
                        </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('admin.gateways.plans.index') }}">لیست پلن ها</a></li>
                        <li><a href="{{ route('admin.gateways.plans.create') }}">پلن جدید</a></li>
                    </ul>
                </li>
                <li><a class="has-arrow  " href="#" aria-expanded="false">
                        <i class="far fa-credit-card"></i>
                        <span
                                class="hide-menu">درگاه ها
                        </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('admin.gateways.index') }}">لیست درگاه ها</a></li>
                        <li><a href="{{ route('admin.gateways.create') }}">درگاه جدید</a></li>
                    </ul>
                </li>
                <li><a class="has-arrow  " href="#" aria-expanded="false">
                        <i class="fas fa-money-bill-alt"></i>
                        <span
                                class="hide-menu">درخواست واریز
                        </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('admin.withdrawals.index') }}">لیست درخواست ها</a></li>
                        <li><a href="{{ route('admin.withdrawals.create') }}">ثبت درخواست</a></li>
                    </ul>
                </li>
                <li><a class="has-arrow  " href="#" aria-expanded="false">
                        <i class="fas fa-file-excel"></i>
                        <span
                                class="hide-menu">گزارش درگاه
                        </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('admin.gateways.reports.index') }}">لیست گزارش ها</a></li>
                    </ul>
                </li>
                <li><a class="has-arrow  " href="#" aria-expanded="false">
                        <i class="fas fa-file-invoice-dollar"></i>
                        <span
                                class="hide-menu">تراکنش های درگاه
                        </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('admin.transactions.gateway.index') }}">لیست تراکنش ها</a></li>
                    </ul>
                </li>
                <li><a class="has-arrow  " href="#" aria-expanded="false">
                        <i class="fas fa-file-invoice-dollar"></i>
                        <span
                                class="hide-menu">تراکنش های بانک
                        </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('admin.transactions.bank.index') }}">لیست تراکنش ها</a></li>
                    </ul>
                </li>
                <li><a class="has-arrow  " href="#" aria-expanded="false">
                        <i class="fas fa-dollar-sign"></i>
                        <span
                                class="hide-menu">پرداخت ها
                        </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('admin.payments.index') }}">لیست پرداخت ها</a></li>
                    </ul>
                </li>
                <li><a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-cog"></i><span
                                class="hide-menu">تنظیمات</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('admin.settings.create') }}">ایجاد</a></li>
                        <li><a href="{{ route('admin.settings.index') }}">لیست</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>
