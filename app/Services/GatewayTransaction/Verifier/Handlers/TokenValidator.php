<?php


namespace App\Services\GatewayTransaction\Verifier\Handlers;


use App\Repositories\Contracts\GatewayRepositoryInterface;
use App\Services\GatewayTransaction\TransactionVerifyRequest;
use App\Services\GatewayTransaction\Verifier\Contracts\Verifier;
use App\Services\GatewayTransaction\Verifier\Exceptions\InvalidTokenException;

class TokenValidator extends Verifier
{

    protected function process(TransactionVerifyRequest $request)
    {
        $gateway_repository = resolve(GatewayRepositoryInterface::class);
        $gateway = $gateway_repository->findBy([
            'gateway_access_token' => $request->getToken()
        ]);
        if(is_null($gateway))
        {
            throw new InvalidTokenException('invalid token!');
        }
        return true;
    }
}