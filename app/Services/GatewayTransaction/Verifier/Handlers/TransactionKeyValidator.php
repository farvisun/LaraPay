<?php


namespace App\Services\GatewayTransaction\Verifier\Handlers;


use App\Repositories\Contracts\GatewayTransactionRepositoryInterface;
use App\Repositories\Eloquent\Transaction\GatewayTransactionStatus;
use App\Services\GatewayTransaction\TransactionVerifyRequest;
use App\Services\GatewayTransaction\Verifier\Contracts\Verifier;
use App\Services\GatewayTransaction\Verifier\Exceptions\InvalidTrasnactionKeyException;

class TransactionKeyValidator extends Verifier
{

    protected function process(TransactionVerifyRequest $request)
    {
        $gatewayTransactionRepository = resolve(GatewayTransactionRepositoryInterface::class);
        $transaction =$gatewayTransactionRepository->findBy([
            'gateway_transaction_key' => $request->getTransactionKey(),
            'gateway_transaction_status' => GatewayTransactionStatus::VERIFY_WAITING
        ]);
        if(is_null($transaction))
        {
            throw new InvalidTrasnactionKeyException('invalid transaction!');
        }
        return true;
    }
}