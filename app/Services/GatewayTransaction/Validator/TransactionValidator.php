<?php


namespace App\Services\GatewayTransaction\Validator;



use App\Services\GatewayTransaction\TransactionRequest;
use App\Services\GatewayTransaction\Validator\Handlers\AmountValidator;
use App\Services\GatewayTransaction\Validator\Handlers\IPValidator;
use App\Services\GatewayTransaction\Validator\Handlers\ResNumberValidator;
use App\Services\GatewayTransaction\Validator\Handlers\TokenValidator;

class TransactionValidator
{
    public function __construct()
    {

    }

    public function validate(TransactionRequest $request)
    {
        $resNumberValidator = new ResNumberValidator();
        $amountValidator = new AmountValidator($resNumberValidator);
//        $ipValidator = new IPValidator($amountValidator);
        $tokenValidator = new TokenValidator($amountValidator);
        return $tokenValidator->handle($request);

    }
}