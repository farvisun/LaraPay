<?php


namespace App\Services\Notification\Providers\Sms\Gateways;


class Parand implements SmsGateway
{

    public function send(string $to, string $message)
    {
        $username = config('sms.drivers.parand.username');
        $number = config('sms.drivers.parand.number');
        $password = config('sms.drivers.parand.password');
        $soap = new \SoapClient('https://182.369.158.24/api/send');
        $soap->sendMessage($number,$username,$password,$to,$message);
    }
}