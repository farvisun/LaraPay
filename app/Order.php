<?php


namespace App;


class Order
{
    protected $products = [];

    public function add(Product $product)
    {
        $this->products[] = $product;
    }

    public function getProducts()
    {
        return $this->products;
    }

    public function getTotalPrice()
    {
        return array_reduce($this->products,function ($current,$product){
            return $current + $product->getPrice();
        });
//        return array_sum(array_map(function($product){
//            return $product->getPrice();
//        },$this->products));
    }

    public function getPaymentUrl()
    {
        return '/order/payment/online';
    }
}