<?php

namespace App\Http\Controllers\Admin\Transaction;

use App\Repositories\Contracts\GatewayTransactionRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GatewayTransactionsController extends Controller
{
    /**
     * @var GatewayTransactionRepositoryInterface
     */
    private $gateway_transaction_repository;

    public function __construct(GatewayTransactionRepositoryInterface $gateway_transaction_repository)
    {

        $this->gateway_transaction_repository = $gateway_transaction_repository;
    }

    public function index()
    {
        $transactions = $this->gateway_transaction_repository->all();

        return view('admin.transaction.gateway.index', compact('transactions'));
    }
}
