<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $table = 'gateway_plans';

    protected $primaryKey = 'gateway_plan_id';

    protected $guarded = ['gateway_plan_id'];

    public function gateways()
    {
        return $this->hasMany(Gateway::class, 'gateway_plan');
    }

    public function add(Gateway $gateway)
    {
        return $this->gateways()->save($gateway);
    }
}
