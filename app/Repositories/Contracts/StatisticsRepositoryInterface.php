<?php


namespace App\Repositories\Contracts;


interface StatisticsRepositoryInterface
{

    public function totalGateways();

    public function todayTotalTransactions();

    public function todayTotalWithdrawal();

    public function totalPendingGateways();

}