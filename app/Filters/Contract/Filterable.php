<?php


namespace App\Filters\Contract;


trait Filterable
{
    public function scopeFilters($query,QueryFilter $filters)
    {
        return $filters->apply($query);
    }
}