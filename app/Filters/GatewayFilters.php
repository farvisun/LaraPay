<?php


namespace App\Filters;


use App\Filters\Contract\QueryFilter;

class GatewayFilters extends QueryFilter
{
    public function sort($value)
    {
        list($type,$order) = explode(',',$value);
        switch ($type)
        {
            case "balance":
                $this->builder->orderBy('gateway_balance',"{$order}");
                break;
        }
    }
}