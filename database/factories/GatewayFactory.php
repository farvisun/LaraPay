<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Gateway::class, function (Faker $faker) {
    return [
        'gateway_plan' => factory(\App\Models\Plan::class)->create()->gateway_plan_id,
        'gateway_user_id' => factory(\App\Models\User::class)->create()->id,
        'gateway_title' => $faker->jobTitle,
        'gateway_website' => 'http://www.7learn.com',
        'gateway_balance' => 0,
        'gateway_access_token' => \App\Helpers\Hash\HashGenerator::make(20),
        'gateway_default_bank' => null,
        'gateway_status' => \App\Models\Gateway::INACTIVE
    ];
});
