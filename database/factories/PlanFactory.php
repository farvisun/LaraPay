<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Plan::class, function (Faker $faker) {
    return [
        'gateway_plan_title' => $faker->randomElement([
            'طلایی',
            'نقره ای',
            'برنزی'
        ]),
        'gateway_plan_commission' => $faker->numberBetween(1,5),
        'gateway_plan_withdrawal_rate' => $faker->numberBetween(3,6),
        'gateway_plan_withdrawal_max' => $faker->numberBetween(1000000,5000000)
,    ];
});
