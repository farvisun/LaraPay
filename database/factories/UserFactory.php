<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$persianFactory = \Faker\Factory::create('fa_IR');
$factory->define(\App\Models\User::class, function (Faker $faker) use ($persianFactory) {
    return [
        'name'           => $persianFactory->name,
        'email'          => $faker->unique()->safeEmail,
        'password'       => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'mobile'         => '09'.$persianFactory->randomElement(['35','36','37','38','39']).rand(1111111,9999999),
        'remember_token' => str_random(10),
    ];
});
